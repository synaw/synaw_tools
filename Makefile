# based on: https://docs.python-guide.org/writing/structure/#makefile

all: dist

init:
	pip install -r requirements.txt

test:
	py.test tests

lint:
	pylint synaw_tools/

dist:
	python setup.py sdist

.PHONY: all init test lint dist
